# Changelog

# [9.1.0](https://gitlab.com/ethima/semantic-release/compare/v9.0.0...v9.1.0) (2024-11-20)


### Features

* **deps:** update dependency @ethima/commitlint-config to v2.1.0 ([3585485](https://gitlab.com/ethima/semantic-release/commit/3585485ab136636bb997c07270b9fa7d51ccf3e3))

# [9.0.0](https://gitlab.com/ethima/semantic-release/compare/v8.0.0...v9.0.0) (2024-10-31)


* build(deps)!: update dependency semantic-release to v24 ([db34f33](https://gitlab.com/ethima/semantic-release/commit/db34f33bfd944cd16905ab4590d5214be72d751a))


### Features

* **commitlint:** enable versioning of shareable configurations ([2f7139f](https://gitlab.com/ethima/semantic-release/commit/2f7139fce4cd75add3c3491291f0aaabfa14a3fa))
* **deps:** migrate to `@ethima/commitlint-config@2.0.0` ([c1b0b12](https://gitlab.com/ethima/semantic-release/commit/c1b0b12d389781ff1e3e3db9fb088c583e81bb76))


### BREAKING CHANGES

* The version of the `semantic-release` tooling this
release ships with (v24.2.0), leverages the `commit-analyzer` and
`release-notes-generator` plugins which both expect to be used with the
latest major versions of conventional-changelog packages. If you are
installing any of these packages in addition to semantic-release, be
sure to update them as well

# [9.0.0-rc.1](https://gitlab.com/ethima/semantic-release/compare/v8.0.0...v9.0.0-rc.1) (2024-10-31)


* build(deps)!: update dependency semantic-release to v24 ([db34f33](https://gitlab.com/ethima/semantic-release/commit/db34f33bfd944cd16905ab4590d5214be72d751a))


### Features

* **commitlint:** enable versioning of shareable configurations ([2f7139f](https://gitlab.com/ethima/semantic-release/commit/2f7139fce4cd75add3c3491291f0aaabfa14a3fa))
* **deps:** migrate to `@ethima/commitlint-config@2.0.0` ([c1b0b12](https://gitlab.com/ethima/semantic-release/commit/c1b0b12d389781ff1e3e3db9fb088c583e81bb76))


### BREAKING CHANGES

* The version of the `semantic-release` tooling this
release ships with (v24.2.0), leverages the `commit-analyzer` and
`release-notes-generator` plugins which both expect to be used with the
latest major versions of conventional-changelog packages. If you are
installing any of these packages in addition to semantic-release, be
sure to update them as well

# [9.0.0-rc.1](https://gitlab.com/ethima/semantic-release/compare/v8.0.0...v9.0.0-rc.1) (2024-10-31)


* build(deps)!: update dependency semantic-release to v24 ([db34f33](https://gitlab.com/ethima/semantic-release/commit/db34f33bfd944cd16905ab4590d5214be72d751a))


### Features

* **commitlint:** enable versioning of shareable configurations ([2f7139f](https://gitlab.com/ethima/semantic-release/commit/2f7139fce4cd75add3c3491291f0aaabfa14a3fa))


### BREAKING CHANGES

* The version of the `semantic-release` tooling this
release ships with (v24.2.0), leverages the `commit-analyzer` and
`release-notes-generator` plugins which both expect to be used with the
latest major versions of conventional-changelog packages. If you are
installing any of these packages in addition to semantic-release, be
sure to update them as well

# [9.0.0-rc.2](https://gitlab.com/ethima/semantic-release/compare/v9.0.0-rc.1...v9.0.0-rc.2) (2024-10-31)

# [9.0.0-rc.1](https://gitlab.com/ethima/semantic-release/compare/v8.0.0...v9.0.0-rc.1) (2024-10-31)


* build(deps)!: update dependency semantic-release to v24 ([0c6e6a7](https://gitlab.com/ethima/semantic-release/commit/0c6e6a71deb246c867be761fbc2d338ee525f584))


### BREAKING CHANGES

* The version of the `semantic-release` tooling this
release ships with (v24.2.0), leverages the `commit-analyzer` and
`release-notes-generator` plugins which both expect to be used with the
latest major versions of conventional-changelog packages. If you are
installing any of these packages in addition to semantic-release, be
sure to update them as well

# [8.0.0](https://gitlab.com/ethima/semantic-release/compare/v7.0.1...v8.0.0) (2024-02-17)


### Bug Fixes

* **commitlint:** stop not linting when `ENABLE_SEMANTIC_RELEASE` is not `"true"` ([0b394f4](https://gitlab.com/ethima/semantic-release/commit/0b394f42998c1f833d7fb4a5d24256d9d12b3e05))


### Build System

* **deps:** update dependency @ethima/semantic-release-configuration to v8 ([1cee643](https://gitlab.com/ethima/semantic-release/commit/1cee643803671795777eaa4873cd8c3ed9a34898))


### Features

* **semantic-release:** support locally defined configurations for extension ([3b917de](https://gitlab.com/ethima/semantic-release/commit/3b917de4cb9b8f57e0e3c5c90bcae83fae93ccf2))


### BREAKING CHANGES

* **deps:** The `@ethima/semantic-release-configuration@8` and
`semantic-release@23` depend on `cosmiconfig@9`. Primarily, this means
that if a project is using a `config.js` file, or an equivalent such as
`config.json` or `config.ts`, to configure `cosmiconfig` itself, the
file needs to be moved into a `.config` directory in the root of the
project.

See `cosmiconfig`'s ["Usage for end users"
documentation][cosmiconfig-meta-config-url] for details and its [v9.0.0
release notes][cosmiconfig-v9-release-notes-url].

[cosmiconfig-meta-config-url]: https://github.com/cosmiconfig/cosmiconfig/blob/a5a842547c13392ebb89a485b9e56d9f37e3cbd3/README.md#usage-for-end-users
[cosmiconfig-v9-release-notes-url]: https://github.com/cosmiconfig/cosmiconfig/releases/tag/v9.0.0

# [8.0.0-rc.1](https://gitlab.com/ethima/semantic-release/compare/v7.0.1...v8.0.0-rc.1) (2024-02-17)


### Bug Fixes

* **commitlint:** stop not linting when `ENABLE_SEMANTIC_RELEASE` is not `"true"` ([0b394f4](https://gitlab.com/ethima/semantic-release/commit/0b394f42998c1f833d7fb4a5d24256d9d12b3e05))


### Build System

* **deps:** update dependency @ethima/semantic-release-configuration to v8 ([1cee643](https://gitlab.com/ethima/semantic-release/commit/1cee643803671795777eaa4873cd8c3ed9a34898))


### Features

* **semantic-release:** support locally defined configurations for extension ([3b917de](https://gitlab.com/ethima/semantic-release/commit/3b917de4cb9b8f57e0e3c5c90bcae83fae93ccf2))


### BREAKING CHANGES

* **deps:** The `@ethima/semantic-release-configuration@8` and
`semantic-release@23` depend on `cosmiconfig@9`. Primarily, this means
that if a project is using a `config.js` file, or an equivalent such as
`config.json` or `config.ts`, to configure `cosmiconfig` itself, the
file needs to be moved into a `.config` directory in the root of the
project.

See `cosmiconfig`'s ["Usage for end users"
documentation][cosmiconfig-meta-config-url] for details and its [v9.0.0
release notes][cosmiconfig-v9-release-notes-url].

[cosmiconfig-meta-config-url]: https://github.com/cosmiconfig/cosmiconfig/blob/a5a842547c13392ebb89a485b9e56d9f37e3cbd3/README.md#usage-for-end-users
[cosmiconfig-v9-release-notes-url]: https://github.com/cosmiconfig/cosmiconfig/releases/tag/v9.0.0

# [8.0.0-rc.2](https://gitlab.com/ethima/semantic-release/compare/v8.0.0-rc.1...v8.0.0-rc.2) (2024-02-17)


### Bug Fixes

* **commitlint:** stop not linting when `ENABLE_SEMANTIC_RELEASE` is not `"true"` ([556008a](https://gitlab.com/ethima/semantic-release/commit/556008a7db1eba66fb41d6dddcc6064e7423ea78))

# [8.0.0-rc.1](https://gitlab.com/ethima/semantic-release/compare/v7.0.1...v8.0.0-rc.1) (2024-02-16)


### Build System

* **deps:** update dependency @ethima/semantic-release-configuration to v8 ([7ff493f](https://gitlab.com/ethima/semantic-release/commit/7ff493fb0bceca644ee2fbce7c276d6d21bf7b25))


### BREAKING CHANGES

* **deps:** The `@ethima/semantic-release-configuration@8` and
`semantic-release@23` depend on `cosmiconfig@9`. Primarily, this means
that if a project is using a `config.js` file, or an equivalent such as
`config.json` or `config.ts`, to configure `cosmiconfig` itself, the
file needs to be moved into a `.config` directory in the root of the
project.

See `cosmiconfig`'s ["Usage for end users"
documentation][cosmiconfig-meta-config-url] for details and its [v9.0.0
release notes][cosmiconfig-v9-release-notes-url].

[cosmiconfig-meta-config-url]: https://github.com/cosmiconfig/cosmiconfig/blob/a5a842547c13392ebb89a485b9e56d9f37e3cbd3/README.md#usage-for-end-users
[cosmiconfig-v9-release-notes-url]: https://github.com/cosmiconfig/cosmiconfig/releases/tag/v9.0.0

## [7.0.1](https://gitlab.com/ethima/semantic-release/compare/v7.0.0...v7.0.1) (2024-01-05)


### Bug Fixes

* **semantic-release:** do not try to release for `build(release):` commits ([54c3e5d](https://gitlab.com/ethima/semantic-release/commit/54c3e5dd53657b47111b6b3720bc01e3b6232c88))

# [7.0.0](https://gitlab.com/ethima/semantic-release/compare/v6.1.2...v7.0.0) (2024-01-03)


### Build System

* **deps:** update dependency @ethima/semantic-release-configuration to v7 ([4286a42](https://gitlab.com/ethima/semantic-release/commit/4286a425fcc108338aee80f4eaf1b905c370208b))


### BREAKING CHANGES

* **deps:** The update to
[@ethima/semantic-release-configuration@7](https://gitlab.com/ethima/semantic-release-configuration)
includes a change to the commit type used for release commits. The type
has been changed from `chore` to `build`. This is only a breaking change
for those with additional workflows on top of the semantic release
workflow provided by this project relying on the structure of these
commit messages.

## [6.1.2](https://gitlab.com/ethima/semantic-release/compare/v6.1.1...v6.1.2) (2023-10-29)


### Bug Fixes

* migrate to centalized Git actor definition ([34b1507](https://gitlab.com/ethima/semantic-release/commit/34b15070e8e60941aa2a698538c2a590550db12c))

## [6.1.1](https://gitlab.com/ethima/semantic-release/compare/v6.1.0...v6.1.1) (2023-10-25)


### Bug Fixes

* **deps:** update node.js to v20.9.0 ([aa348ba](https://gitlab.com/ethima/semantic-release/commit/aa348ba2db2b71e4b7277c06d00ff5f477bb3000))

# [6.1.0](https://gitlab.com/ethima/semantic-release/compare/v6.0.1...v6.1.0) (2023-10-24)


### Features

* **deps:** update node.js to v20 ([1055dbf](https://gitlab.com/ethima/semantic-release/commit/1055dbf4d01c037a6a89cc990d3374b4463fa3b6))

## [6.0.1](https://gitlab.com/ethima/semantic-release/compare/v6.0.0...v6.0.1) (2023-07-28)


### Bug Fixes

* **workflows:** add missing `BRANCH_PREFIX_SEPARATOR` ([fbfe8c8](https://gitlab.com/ethima/semantic-release/commit/fbfe8c8787579efef4966cf7f3022ed7c3bb6f52))

# [6.0.0](https://gitlab.com/ethima/semantic-release/compare/v5.0.0...v6.0.0) (2023-07-28)


### Bug Fixes

* **deps:** update dependency ethima/gitlab-ci to v7 ([ece8b0b](https://gitlab.com/ethima/semantic-release/commit/ece8b0b4dadae1b68f1f9386698aff1cf12857a2))

# [5.0.0](https://gitlab.com/ethima/semantic-release/compare/v4.0.2...v5.0.0) (2023-07-27)


### Features

* allow configuration using GitLab "Ethima Tokens" ([dd9c07e](https://gitlab.com/ethima/semantic-release/commit/dd9c07e77228bb3a483a0b52691faae2181a9e1a))
* allow configuration using NPM "Ethima Tokens" ([b041cf7](https://gitlab.com/ethima/semantic-release/commit/b041cf76966f4820b4e37eceb272e39205d01686))
* commit and tag as "Ethima Bot" ([6576f23](https://gitlab.com/ethima/semantic-release/commit/6576f233c0321afadb1a23acb3d51a9e12d262a9))
* **deps:** update dependency @ethima/semantic-release-configuration to v4.1.0 ([85bec56](https://gitlab.com/ethima/semantic-release/commit/85bec569c41998ae2bce7eedd5d0e956bbdc5113))
* **process:** trigger `Semantic Release` for the `Maintenance Branch (Push)` scenario ([2097152](https://gitlab.com/ethima/semantic-release/commit/2097152cd6c35f212cfa718525c5428b9a877073))
* **process:** trigger `Semantic Release` for the `Prerelease Branch (Push)` scenario ([bec46b3](https://gitlab.com/ethima/semantic-release/commit/bec46b32a007dcc90ae7df1ab4c10274c935203e))

# [5.0.0-rc.2](https://gitlab.com/ethima/semantic-release/compare/v5.0.0-rc.1...v5.0.0-rc.2) (2023-07-26)


### Features

* allow configuration using GitLab "Ethima Tokens" ([dd9c07e](https://gitlab.com/ethima/semantic-release/commit/dd9c07e77228bb3a483a0b52691faae2181a9e1a))
* allow configuration using NPM "Ethima Tokens" ([b041cf7](https://gitlab.com/ethima/semantic-release/commit/b041cf76966f4820b4e37eceb272e39205d01686))

# [5.0.0-rc.1](https://gitlab.com/ethima/semantic-release/compare/v4.0.2...v5.0.0-rc.1) (2023-07-15)


### Features

* commit and tag as "Ethima Bot" ([6576f23](https://gitlab.com/ethima/semantic-release/commit/6576f233c0321afadb1a23acb3d51a9e12d262a9))
* **deps:** update dependency @ethima/semantic-release-configuration to v4.1.0 ([85bec56](https://gitlab.com/ethima/semantic-release/commit/85bec569c41998ae2bce7eedd5d0e956bbdc5113))
* **process:** trigger `Semantic Release` for the `Maintenance Branch (Push)` scenario ([2097152](https://gitlab.com/ethima/semantic-release/commit/2097152cd6c35f212cfa718525c5428b9a877073))
* **process:** trigger `Semantic Release` for the `Prerelease Branch (Push)` scenario ([bec46b3](https://gitlab.com/ethima/semantic-release/commit/bec46b32a007dcc90ae7df1ab4c10274c935203e))

## [4.0.2](https://gitlab.com/ethima/semantic-release/compare/v4.0.1...v4.0.2) (2023-07-11)


### Bug Fixes

* **process:** trigger for the `Manual` scenario ([e827d62](https://gitlab.com/ethima/semantic-release/commit/e827d623fd0b2950692c234783be599681135a45))
* **process:** trigger for the `Release` scenario ([2663f92](https://gitlab.com/ethima/semantic-release/commit/2663f92d0b930b37cda99c2effc4d9b9cc252b5c))
* **process:** trigger for the `Schedule` scenario ([ec68741](https://gitlab.com/ethima/semantic-release/commit/ec68741fd297e0378112d3e18a2cb9010218a15c))

## [4.0.1](https://gitlab.com/ethima/semantic-release/compare/v4.0.0...v4.0.1) (2023-05-05)


### Bug Fixes

* **deps:** migrate to the `Default Branch (Push)` workflow ([dc4c5fe](https://gitlab.com/ethima/semantic-release/commit/dc4c5fea433ea1e731e775b026b0c2b852d920c1))
* **process:** enable semantic releases on pushes to the default branch ([ad654ef](https://gitlab.com/ethima/semantic-release/commit/ad654efab15b0acc51960240bd0bdc84b52dfbfa))

# [4.0.0](https://gitlab.com/ethima/semantic-release/compare/v3.2.0...v4.0.0) (2023-04-26)


* feat!(deps): update dependency @ethima/semantic-release-configuration to v4 ([6ebbc1d](https://gitlab.com/ethima/semantic-release/commit/6ebbc1d0a3dfe3b041360243252aaf1d4e57d90b)), closes [/gitlab.com/ethima/semantic-release-configuration/-/blob/c420d14c7e66d6227a04e54ad116fd2654f8d215/CHANGELOG.md#400-2023-04-25](https://gitlab.com//gitlab.com/ethima/semantic-release-configuration/-/blob/c420d14c7e66d6227a04e54ad116fd2654f8d215/CHANGELOG.md/issues/400-2023-04-25)


### BREAKING CHANGES

* The tokens used by the
`@ethima/semantic-release-configuration` to annotate versioned templates
have been changed[^1].

The previously used `<!-- START_VERSIONED_TEMPLATE` and `<!--
END_VERSIONED_TEMPLATE -->` tokens are replaced with the
`BEGIN_VERSIONED_TEMPLATE` and `END_VERSIONED_TEMPLATE_REPLACEMENT`
tokens respectively. The `END_VERSIONED_TEMPLATE` token now indicates
the end of the template, not the end of the replaced content.

# [3.2.0](https://gitlab.com/ethima/semantic-release/compare/v3.1.0...v3.2.0) (2023-02-14)


### Features

* **deps:** update dependency @ethima/semantic-release-configuration to v3.3.0 ([e3c4c49](https://gitlab.com/ethima/semantic-release/commit/e3c4c495a7a24fbbe786897982c2e770cd241e1e))

# [3.1.0](https://gitlab.com/ethima/semantic-release/compare/v3.0.0...v3.1.0) (2023-02-03)


### Features

* **semantic-release:** enable configuration of the semantic release version ([af266c3](https://gitlab.com/ethima/semantic-release/commit/af266c3d11e1c4b880e04b8a7912fa12626c825a))

# [3.0.0](https://gitlab.com/ethima/semantic-release/compare/v2.0.1...v3.0.0) (2023-01-18)


### chore

* **deps:** update dependency @ethima/semantic-release-configuration to v3 ([253a0e9](https://gitlab.com/ethima/semantic-release/commit/253a0e91df131080fc4156ba1f3b16cb4c9e07aa))


### Features

* **commitlint:** provide prefilled variable hints for manual pipelines ([e9436eb](https://gitlab.com/ethima/semantic-release/commit/e9436eb8d24c35d140ab453bb3e0826573be3359))
* **semantic-release:** provide prefilled variable hints for manual pipelines ([8561e18](https://gitlab.com/ethima/semantic-release/commit/8561e1820a916f2f80088b6a661e9df17584a1df))


### BREAKING CHANGES

* **deps:** The ethima/semantic-release-configuration> has switched
to the [Conventional Commits](https://conventionalcommits.org) preset
for the semantic release tooling. This should be mostly backwards
compatible with the previously used
[`angular`][semantic-release-preset-angular], but is considered a
breacking change. This propagates through this process as the
configuration is the default for it.

[semantic-release-preset-conventionalcommits]: https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-conventionalcommits
[semantic-release-preset-angular]: https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular

## [2.0.1](https://gitlab.com/ethima/semantic-release/compare/v2.0.0...v2.0.1) (2023-01-06)


### Bug Fixes

* **deps:** update dependency @ethima/semantic-release-configuration to v2.1.0 ([bd926f5](https://gitlab.com/ethima/semantic-release/commit/bd926f5b1345b59ea46dd5f9f5befed74d89d216))

# [2.0.0](https://gitlab.com/ethima/semantic-release/compare/v1.2.3...v2.0.0) (2022-12-30)

- feat!: migrate to the @ethima organization ([f8ec21c](https://gitlab.com/ethima/semantic-release/commit/f8ec21c0db83c031c8782610841b21c0d5889abd))

### Bug Fixes

- **deps:** upgrade to `@ethima/semantic-release-configuration@2.0.0` ([c06f396](https://gitlab.com/ethima/semantic-release/commit/c06f39671087a7a7ee19f527c57ca3e06532a967))

### BREAKING CHANGES

- The project has been migrated from @bauglir's personal
  namespace to the @ethima namespace, which is intended to host a variety
  of process-related projects, and renamed accordingly. It is now more
  succinctly known as ethima/semantic-release>.

## [1.2.3](https://gitlab.com/ethima/semantic-release/compare/v1.2.2...v1.2.3) (2022-12-28)

### Bug Fixes

- include process name in generated pipelines ([e443c66](https://gitlab.com/ethima/semantic-release/commit/e443c6619c9a95b545b4dbe1890cb143d812d77c))

## [1.2.2](https://gitlab.com/ethima/semantic-release/compare/v1.2.1...v1.2.2) (2022-12-23)

### Bug Fixes

- **commitlint:** disable when the primary semantic release job is disabled ([74539b5](https://gitlab.com/ethima/semantic-release/commit/74539b5590a1cb97b1e397094b7766bf89a46b88))

## [1.2.1](https://gitlab.com/ethima/semantic-release/compare/v1.2.0...v1.2.1) (2022-12-22)

### Bug Fixes

- **deps:** upgrade to `@bauglir/semantic-release-configuration@1.1.0` ([65dccf6](https://gitlab.com/ethima/semantic-release/commit/65dccf67e2479a888d1ba76bfafd070019358522))
- **deps:** upgrade to `@bauglir/semantic-release-configuration@1.1.1` ([bbb1c5b](https://gitlab.com/ethima/semantic-release/commit/bbb1c5b73cefd290d1a4cb4402c00ae42fd0827a))

# [1.2.0](https://gitlab.com/ethima/semantic-release/compare/v1.1.0...v1.2.0) (2022-12-21)

### Bug Fixes

- **process:** disable commit message linting on the default branch ([68362b9](https://gitlab.com/ethima/semantic-release/commit/68362b945da71f1e59b731a1586d633681296b81))

### Features

- **process:** do not semantically release for merge requests ([7e0118b](https://gitlab.com/ethima/semantic-release/commit/7e0118b0aefc84e978631812db73cc7849f03988))
- **process:** enable reuse of workflow definitions ([45c1f3b](https://gitlab.com/ethima/semantic-release/commit/45c1f3b6688d99f8695ca550fe5a1a729fe7c969))

# [1.1.0](https://gitlab.com/ethima/semantic-release/compare/v1.0.1...v1.1.0) (2022-11-11)

### Features

- **commitlint:** verify commit messages adhere to a standard format ([eed68cf](https://gitlab.com/ethima/semantic-release/commit/eed68cf55690bcc47de4d26f9937c4a545fa8c1b))

## [1.0.1](https://gitlab.com/ethima/semantic-release/compare/v1.0.0...v1.0.1) (2022-11-10)

### Bug Fixes

- **semantic-release:** do not try to release based on a release commit ([ad9e91e](https://gitlab.com/ethima/semantic-release/commit/ad9e91e7f4891c3305ac60eb348062138da105d3))

# 1.0.0 (2022-11-08)

### Features

- create semantic releases from GitLab CI ([b6765eb](https://gitlab.com/ethima/semantic-release/commit/b6765eb09e1824e6c86674a89eb0a13467ff8b79))
- **process:** add process to semantically release from the default branch ([9cbb858](https://gitlab.com/ethima/semantic-release/commit/9cbb858636936b70b9c7295eae9c501479c10b0f))
